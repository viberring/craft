import { Button } from 'craft-ui/dist/src';

function CraftButton() {
    return (
        <>
            <button>Button</button>
            <Button label="craft-button" />
        </>
    );
}

export default CraftButton;